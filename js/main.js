var numRow =1;

function formatNumber(n) {
    return n.toFixed(0).replace(/./g, function(c, i, a) {
      return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
    });
}

const changeQtyItem = (key, set) => {
    let qty = parseInt($(`.qty-${key}`).text().replace(/\./g, ""))
    $(`.qty-${key}`).text("")
    if(set == "minus"){
        $(`.qty-${key}`).text(formatNumber(--qty))
        if(qty < 1){ deleteItem(key) }
    }else{
        $(`.qty-${key}`).text(formatNumber(++qty))
    }
    changeTotal()
}

const addTr = ()=>{
    numRow++
    $('tbody').append(`
        <tr class="item item-${numRow}">
            
            <td>${numRow}</td>
            <td><span onkeyup="changeTotal()" contenteditable>...</span></td>
            <td>
                <span>&#36;</span>
                <span onkeyup="changeTotal()" class="price" contenteditable>0</span>
            </td>
            <td>
                <span onclick="changeQtyItem(${numRow}, 'minus')" class="lnr lnr-circle-minus"></span>
                <span class="qty qty-${numRow}" contenteditable>1</span>
                <span onclick="changeQtyItem( ${numRow}, 'add')" class="lnr lnr-plus-circle"></span>
            </td>
            <td>
                <span>&#36;</span>
                <span class="subTotal">0</span>
            </td>
            <td>
                <button onclick="deleteItem(${numRow})" class="btn btn-outline-danger btn-sm">x</button>
            </td>
        </tr>
    `)
}

const deleteItem = (key) => {
    $(`.item-${key}`).remove()
}

const changeTotal = () => {
    let items = $(".item")
    let qty = $(".qty")
    let price = $(".price")
    let subTotal = $(".subTotal")
    var total = 0;
    items.map((index)=>{
        let a = parseInt(qty.eq(index).text().replace(/\./g, ""))
        let b = parseInt(price.eq(index).text().replace(/\./g, "")) 
        subTotal.eq(index).text(formatNumber(a*b))
        total += a*b
    })
    $(".total").text(formatNumber(total))
}
    
window.onload = changeTotal()

$('div[contenteditable]').keydown(function(e) {
    // trap the return key being pressed
    if (e.keyCode === 13) {
        // insert 2 br tags (if only one br tag is inserted the cursor won't go to the next line)
        document.execCommand('insertHTML', false, '<br/>');
        // prevent the default behaviour of return key pressed
        return false;
    }
});